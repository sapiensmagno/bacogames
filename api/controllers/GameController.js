/**
 * GameController
 *
 * @description :: Server-side logic for managing games
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	join: function (req, res) {
        
        var Promise = require("bluebird");
        var join = Promise.join;

        join(getRoom(req.param('room_name')), getPlayer(req.param('player_name')), createGame)
        .catch(errHandler);
        
        function createGame (gameRoom, gamePlayer) {
            Game.create({player: gamePlayer, room: gameRoom})
            .then(renderPage)
            .catch(errHandler);
        }

        function errHandler (e) {
            console.log('Error at GameController!');
            console.log(e);
            return res.serverError(e);
        }

        function renderPage(game) {
            join(getGame(game), getRoom(game.room.name), getPlayer(game.player.name), function (g, tRoom, player) {
                res.render('tastingRoom', {game: g, tastingRoom: tRoom, currentPlayer: player})
            })
            .catch(errHandler);
        }
        
        function getGame(game) {
            return Game.findOne(game)
            .populate('room')
            .populate('player');
        }

        function getRoom(roomName) {
            return TastingRoom.findOne({name : roomName})
            .populate('players');
        }

        function getPlayer(playername) {
            return Player.findOne({name: playername})
            .populate('games');
        }
       
        //player.name = { name: req.param('player_name')};
        //return res.render('tastingRoom', {tastingRoom: tastingRoomz, player: player});
        //return res.send (player.name);//tastingRoom.totalRounds();
    }
};
// tastingRoomz.create().exec(function (err, obj) {
//             console.log('couldnt create');
//         });