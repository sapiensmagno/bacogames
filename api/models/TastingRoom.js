/**
 * TastingRoom.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    name: { 
      type: 'string',
      minLength: 4,
      unique: true,
      required: true,
      notNull: true
    },
    isOpen: { 
      type: 'boolean',
      defaultsTo: true
    },
    totalRounds: {
      type: 'integer',
      defaultsTo: 5,
      min: 1
    },
    games: {
      collection:'Game',
      via: 'room'
    },
    players: {
      collection: 'Player',
      via: 'player',
      through: 'game'
    }
  }
};

