/**
 * Game.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    score: { 
      type: 'integer',
      defaultsTo: 0
    },
    round: {
      type: 'integer',
      defaultsTo: 1
    },
    isFinished: { 
      type: 'boolean'
    },
    player: {
      model: 'Player',
      required: true
    },
    room: { 
      model: 'TastingRoom',
      required: true
    },    
  }
};

